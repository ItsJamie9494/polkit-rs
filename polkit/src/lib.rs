#![cfg_attr(feature = "dox", feature(doc_cfg))]
#![allow(clippy::needless_doctest_main)]
//! # Rust Polkit Bindings
//!
//! This library contains safe Rust bindings for [Polkit](https://gitlab.freedesktop.org/polkit/polkit), a toolkit for defining and handling authorizations.
//!
//! See also
//!
//! - [PolkitAgent Rust bindings documentation](mod@polkit-agent)
//!

// Re-export the -sys bindings
pub use ffi;
pub use gio;
pub use glib;

// Polkit has no runtime to initialize
macro_rules! assert_initialized_main_thread {
    () => {};
}

// No-op
macro_rules! skip_assert_initialized {
    () => {};
}

#[allow(unused_imports)]
#[allow(clippy::let_and_return)]
#[allow(clippy::type_complexity)]
mod auto;
mod implicit_authorization;

pub use auto::*;
pub use implicit_authorization::*;

pub mod prelude {
    pub use crate::auto::traits::*;
}

pub mod builders {
    pub use crate::auto::builders::*;
}
