// This file was generated by gir (https://github.com/gtk-rs/gir)
// from ..
// from ../gir-files
// DO NOT EDIT

use crate::{Subject};
use glib::{prelude::*,translate::*};
use std::{boxed::Box as Box_,fmt,pin::Pin,ptr};

glib::wrapper! {
    #[doc(alias = "PolkitPermission")]
    pub struct Permission(Object<ffi::PolkitPermission>) @extends gio::Permission, @implements gio::AsyncInitable, gio::Initable;

    match fn {
        type_ => || ffi::polkit_permission_get_type(),
    }
}

impl Permission {
    #[doc(alias = "polkit_permission_new_sync")]
    pub fn new_sync(action_id: &str, subject: Option<&impl IsA<Subject>>, cancellable: Option<&impl IsA<gio::Cancellable>>) -> Result<Permission, glib::Error> {
        assert_initialized_main_thread!();
        unsafe {
            let mut error = ptr::null_mut();
            let ret = ffi::polkit_permission_new_sync(action_id.to_glib_none().0, subject.map(|p| p.as_ref()).to_glib_none().0, cancellable.map(|p| p.as_ref()).to_glib_none().0, &mut error);
            if error.is_null() { Ok(gio::Permission::from_glib_full(ret).unsafe_cast()) } else { Err(from_glib_full(error)) }
        }
    }

            // rustdoc-stripper-ignore-next
            /// Creates a new builder-pattern struct instance to construct [`Permission`] objects.
            ///
            /// This method returns an instance of [`PermissionBuilder`](crate::builders::PermissionBuilder) which can be used to create [`Permission`] objects.
            pub fn builder() -> PermissionBuilder {
                PermissionBuilder::new()
            }
        

    #[doc(alias = "polkit_permission_get_action_id")]
    #[doc(alias = "get_action_id")]
    pub fn action_id(&self) -> glib::GString {
        unsafe {
            from_glib_none(ffi::polkit_permission_get_action_id(self.to_glib_none().0))
        }
    }

    #[doc(alias = "polkit_permission_get_subject")]
    #[doc(alias = "get_subject")]
    pub fn subject(&self) -> Subject {
        unsafe {
            from_glib_none(ffi::polkit_permission_get_subject(self.to_glib_none().0))
        }
    }

    #[doc(alias = "polkit_permission_new")]
    pub fn new<P: FnOnce(Result<gio::Permission, glib::Error>) + 'static>(action_id: &str, subject: Option<&impl IsA<Subject>>, cancellable: Option<&impl IsA<gio::Cancellable>>, callback: P) {
        assert_initialized_main_thread!();
        
                let main_context = glib::MainContext::ref_thread_default();
                let is_main_context_owner = main_context.is_owner();
                let has_acquired_main_context = (!is_main_context_owner)
                    .then(|| main_context.acquire().ok())
                    .flatten();
                assert!(
                    is_main_context_owner || has_acquired_main_context.is_some(),
                    "Async operations only allowed if the thread is owning the MainContext"
                );
        
        let user_data: Box_<glib::thread_guard::ThreadGuard<P>> = Box_::new(glib::thread_guard::ThreadGuard::new(callback));
        unsafe extern "C" fn new_trampoline<P: FnOnce(Result<gio::Permission, glib::Error>) + 'static>(_source_object: *mut glib::gobject_ffi::GObject, res: *mut gio::ffi::GAsyncResult, user_data: glib::ffi::gpointer) {
            let mut error = ptr::null_mut();
            let ret = ffi::polkit_permission_new_finish(res, &mut error);
            let result = if error.is_null() { Ok(from_glib_full(ret)) } else { Err(from_glib_full(error)) };
            let callback: Box_<glib::thread_guard::ThreadGuard<P>> = Box_::from_raw(user_data as *mut _);
            let callback: P = callback.into_inner();
            callback(result);
        }
        let callback = new_trampoline::<P>;
        unsafe {
            ffi::polkit_permission_new(action_id.to_glib_none().0, subject.map(|p| p.as_ref()).to_glib_none().0, cancellable.map(|p| p.as_ref()).to_glib_none().0, Some(callback), Box_::into_raw(user_data) as *mut _);
        }
    }

    
    pub fn new_future(action_id: &str, subject: Option<&(impl IsA<Subject> + Clone + 'static)>) -> Pin<Box_<dyn std::future::Future<Output = Result<gio::Permission, glib::Error>> + 'static>> {

        skip_assert_initialized!();
        let action_id = String::from(action_id);
        let subject = subject.map(ToOwned::to_owned);
        Box_::pin(gio::GioFuture::new(&(), move |_obj, cancellable, send| {
            Self::new(
                &action_id,
                subject.as_ref().map(::std::borrow::Borrow::borrow),
                Some(cancellable),
                move |res| {
                    send.resolve(res);
                },
            );
        }))
    }
}

// rustdoc-stripper-ignore-next
        /// A [builder-pattern] type to construct [`Permission`] objects.
        ///
        /// [builder-pattern]: https://doc.rust-lang.org/1.0.0/style/ownership/builders.html
#[must_use = "The builder must be built to be used"]
pub struct PermissionBuilder {
            builder: glib::object::ObjectBuilder<'static, Permission>,
        }

        impl PermissionBuilder {
        fn new() -> Self {
            Self { builder: glib::object::Object::builder() }
        }

                            pub fn action_id(self, action_id: impl Into<glib::GString>) -> Self {
                            Self { builder: self.builder.property("action-id", action_id.into()), }
                        }

                            pub fn subject(self, subject: &impl IsA<Subject>) -> Self {
                            Self { builder: self.builder.property("subject", subject.clone().upcast()), }
                        }

    // rustdoc-stripper-ignore-next
    /// Build the [`Permission`].
    #[must_use = "Building the object from the builder is usually expensive and is not expected to have side effects"]
    pub fn build(self) -> Permission {
    self.builder.build() }
}

impl fmt::Display for Permission {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Permission")
    }
}
