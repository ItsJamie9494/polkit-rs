use glib::translate::{from_glib, IntoGlib, ToGlibPtr};

use crate::ImplicitAuthorization;

impl ImplicitAuthorization {
    #[doc(alias = "polkit_implicit_authorization_from_string")]
    pub fn from_string(string: &str, out_implicit_authorization: &ImplicitAuthorization) -> bool {
        assert_initialized_main_thread!();
        unsafe {
            from_glib(ffi::polkit_implicit_authorization_from_string(
                string.to_glib_none().0,
                &mut out_implicit_authorization.into_glib(),
            ))
        }
    }
}
