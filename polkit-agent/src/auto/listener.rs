// This file was generated by gir (https://github.com/gtk-rs/gir)
// from ..
// from ../gir-files
// DO NOT EDIT

use glib::{prelude::*,translate::*};
use std::{fmt,ptr};

glib::wrapper! {
    #[doc(alias = "PolkitAgentListener")]
    pub struct Listener(Object<ffi::PolkitAgentListener, ffi::PolkitAgentListenerClass>);

    match fn {
        type_ => || ffi::polkit_agent_listener_get_type(),
    }
}

impl Listener {
        pub const NONE: Option<&'static Listener> = None;
    
}

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::Listener>> Sealed for T {}
}

pub trait ListenerExt: IsA<Listener> + sealed::Sealed + 'static {}

impl<O: IsA<Listener>> ListenerExt for O {}

impl fmt::Display for Listener {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Listener")
    }
}
