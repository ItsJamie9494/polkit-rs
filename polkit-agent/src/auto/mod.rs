// This file was generated by gir (https://github.com/gtk-rs/gir)
// from ..
// from ../gir-files
// DO NOT EDIT

mod listener;
pub use self::listener::Listener;

mod session;
pub use self::session::Session;

mod text_listener;
pub use self::text_listener::TextListener;

mod flags;
pub use self::flags::RegisterFlags;

pub mod functions;

#[doc(hidden)]
pub mod traits {
    pub use super::listener::ListenerExt;
}
#[doc(hidden)]
pub mod builders {
    pub use super::session::SessionBuilder;
    pub use super::text_listener::TextListenerBuilder;
}
