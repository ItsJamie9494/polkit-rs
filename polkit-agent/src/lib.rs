#![cfg_attr(feature = "dox", feature(doc_cfg))]
#![allow(clippy::needless_doctest_main)]
//! # Rust PolkitAgent Bindings
//!
//! This library contains safe Rust bindings for [PolkitAgent](https://gitlab.freedesktop.org/polkit/polkit).
//!
//! See also
//!
//! - [Polkit Rust bindings documentation](mod@polkit)
//!

// Re-export the -sys bindings
pub use ffi;
pub use gio;
pub use glib;
pub use polkit;

// PolkitAgent has no runtime to initialize
macro_rules! assert_initialized_main_thread {
    () => {};
}

// No-op
macro_rules! skip_assert_initialized {
    () => {};
}

#[allow(unused_imports)]
#[allow(clippy::let_and_return)]
#[allow(clippy::type_complexity)]
mod auto;
mod listener;

pub use auto::functions::*;
pub use auto::*;

pub mod prelude {
    pub use crate::auto::traits::*;
    pub use crate::listener::ListenerExtManual;
}

pub mod builders {
    pub use crate::auto::builders::*;
}
