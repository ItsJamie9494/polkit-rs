use glib::{
    translate::{from_glib_full, FromGlibPtrFull, IntoGlib, ToGlibPtr},
    IsA,
};

use crate::{Listener, RegisterFlags};
use std::{boxed::Box as Box_, pin::Pin, ptr};

pub struct ListenerServer {
    ptr: glib::Pointer,
}

impl Listener {
    #[doc(alias = "polkit_agent_listener_unregister")]
    pub fn unregister(registration_handle: ListenerServer) {
        unsafe { ffi::polkit_agent_listener_unregister(registration_handle.ptr) }
    }
}

pub trait ListenerExtManual: 'static {
    fn initiate_authentication<P: FnOnce(Result<(), glib::Error>) + 'static>(
        &self,
        action_id: &str,
        message: &str,
        icon_name: &str,
        details: &polkit::Details,
        cookie: &str,
        identities: &[polkit::Identity],
        cancellable: Option<&impl IsA<gio::Cancellable>>,
        callback: P,
    );

    fn initiate_authentication_future(
        &self,
        action_id: &str,
        message: &str,
        icon_name: &str,
        details: &polkit::Details,
        cookie: &str,
        identities: &[polkit::Identity],
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>>;

    fn register(
        &self,
        flags: RegisterFlags,
        subject: &impl IsA<polkit::Subject>,
        object_path: &str,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<Option<ListenerServer>, glib::Error>;

    fn register_with_options(
        &self,
        flags: RegisterFlags,
        subject: &impl IsA<polkit::Subject>,
        object_path: &str,
        options: Option<&glib::Variant>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<Option<ListenerServer>, glib::Error>;
}

impl<O: IsA<Listener>> ListenerExtManual for O {
    #[doc(alias = "polkit_agent_listener_register")]
    fn register(
        &self,
        flags: RegisterFlags,
        subject: &impl IsA<polkit::Subject>,
        object_path: &str,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<Option<ListenerServer>, glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let pointer = ffi::polkit_agent_listener_register(
                self.as_ref().to_glib_none().0,
                flags.into_glib(),
                subject.as_ref().to_glib_none().0,
                object_path.to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );

            if pointer.is_null() {
                return Err(glib::Error::from_glib_full(error));
            }

            return Ok(Some(ListenerServer { ptr: pointer }));
        }
    }

    #[doc(alias = "polkit_agent_listener_register_with_options")]
    fn register_with_options(
        &self,
        flags: RegisterFlags,
        subject: &impl IsA<polkit::Subject>,
        object_path: &str,
        options: Option<&glib::Variant>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<Option<ListenerServer>, glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let pointer = ffi::polkit_agent_listener_register_with_options(
                self.as_ref().to_glib_none().0,
                flags.into_glib(),
                subject.as_ref().to_glib_none().0,
                object_path.to_glib_none().0,
                options.to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );

            if pointer.is_null() {
                return Err(glib::Error::from_glib_full(error));
            }

            return Ok(Some(ListenerServer { ptr: pointer }));
        }
    }

    #[doc(alias = "polkit_agent_listener_initiate_authentication")]
    fn initiate_authentication<P: FnOnce(Result<(), glib::Error>) + 'static>(
        &self,
        action_id: &str,
        message: &str,
        icon_name: &str,
        details: &polkit::Details,
        cookie: &str,
        identities: &[polkit::Identity],
        cancellable: Option<&impl IsA<gio::Cancellable>>,
        callback: P,
    ) {
        let main_context = glib::MainContext::ref_thread_default();
        let is_main_context_owner = main_context.is_owner();
        let has_acquired_main_context = (!is_main_context_owner)
            .then(|| main_context.acquire().ok())
            .flatten();
        assert!(
            is_main_context_owner || has_acquired_main_context.is_some(),
            "Async operations only allowed if the thread is owning the MainContext"
        );

        let user_data: Box_<glib::thread_guard::ThreadGuard<P>> =
            Box_::new(glib::thread_guard::ThreadGuard::new(callback));
        unsafe extern "C" fn initiate_authentication_trampoline<
            P: FnOnce(Result<(), glib::Error>) + 'static,
        >(
            _source_object: *mut glib::gobject_ffi::GObject,
            res: *mut gio::ffi::GAsyncResult,
            user_data: glib::ffi::gpointer,
        ) {
            let mut error = ptr::null_mut();
            let _ = ffi::polkit_agent_listener_initiate_authentication_finish(
                _source_object as *mut _,
                res,
                &mut error,
            );
            let result = if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            };
            let callback: Box_<glib::thread_guard::ThreadGuard<P>> =
                Box_::from_raw(user_data as *mut _);
            let callback: P = callback.into_inner();
            callback(result);
        }
        let callback = initiate_authentication_trampoline::<P>;
        unsafe {
            ffi::polkit_agent_listener_initiate_authentication(
                self.as_ref().to_glib_none().0,
                action_id.to_glib_none().0,
                message.to_glib_none().0,
                icon_name.to_glib_none().0,
                details.to_glib_none().0,
                cookie.to_glib_none().0,
                identities.to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                Some(callback),
                Box_::into_raw(user_data) as *mut _,
            );
        }
    }

    fn initiate_authentication_future(
        &self,
        action_id: &str,
        message: &str,
        icon_name: &str,
        details: &polkit::Details,
        cookie: &str,
        identities: &[polkit::Identity],
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>> {
        let action_id = String::from(action_id);
        let message = String::from(message);
        let icon_name = String::from(icon_name);
        let details = details.clone();
        let cookie = String::from(cookie);
        let identities = identities.to_vec();
        Box_::pin(gio::GioFuture::new(self, move |obj, cancellable, send| {
            obj.initiate_authentication(
                &action_id,
                &message,
                &icon_name,
                &details,
                &cookie,
                &identities.as_slice(),
                Some(cancellable),
                move |res| {
                    send.resolve(res);
                },
            );
        }))
    }
}
